package contactless;
//Singleton: Objekt wird in der main mit .getInstance() erstellt.

public class ContactlessServer {
	//private static TripRecorder tripRecorder = null; 
	
	private static ContactlessServer instance;
	
	private ContactlessServer() {
		
	}
	
	public static ContactlessServer getInstance() {
		if(instance == null) {
			instance = new ContactlessServer();
		}
		
		return instance;
		
	}
	
	
	
}
